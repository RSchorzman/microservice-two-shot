# Wardrobify

Team:

* Riley Schorzman - Hat
* Steven Ericksen - Shoe

## Design

## Shoes microservice

The shoe model stores the required information for each shoe (manufacturer, model-name, color, picture-url, and what bin they're in). It works with wardrobify through the use of BinVO. The poller pulls from the wardrobe's Bin model and passes the data through BinVO for use in the microservice.

Add a shoe has it's own form where the user can enter the fields for the shoe they are adding. Upon successful adding of a shoe they are redirected to the shoe list which shoes the image of the shoe, and all of the data. The shoe list is where the functional delete is located.

The shoe list page is displayed in card form, each shoe on it's own card with all data.

## Hats microservice

    Created two models, the hats model to decide the required
elementsof a hat along with the foreign key of locations using
locationVO, and the locationVO to efficiently impliment information
from thelocations model in the wardrobe microservice into the
hats model.
    There is a view for listing the hats with the ability to
create a new hat, and a view for the details of the hats with
the ability toupdate and delete hats, each using varying model
encoders of the models.
    In react the listing, details, and deletion of the hats is
all done in the Hats.js, while the creation is done in the NewHat.js.
    (Also suprises for deleting items thanks to Steven).

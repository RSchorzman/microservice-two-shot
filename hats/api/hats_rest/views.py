from django.shortcuts import render
from .models import Hats, LocationVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["name", "style", "pk"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "pk",
        "name",
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
            return JsonResponse(
                {"hats": hats},
                encoder=HatsListEncoder,
            )
        else:
            hats = Hats.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatsListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            print(location.import_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            print(location.import_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

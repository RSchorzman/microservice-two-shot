from django.db import models
import requests
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(max_length=500, null=True)
    bin = models.ForeignKey(
        BinVO,
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("shoe_list", kwargs={"pk": self.pk})

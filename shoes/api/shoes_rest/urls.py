from django.urls import path
from . import views

urlpatterns = [
    path('shoes/', views.shoe_list, name='shoe-list'),
    path('shoes/<int:pk>/', views.shoe_detail, name='shoe-detail'),
]

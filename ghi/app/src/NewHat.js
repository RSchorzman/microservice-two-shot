import React, {useState, useEffect} from "react";
import { useNavigate } from "react-router-dom";

function HatForm(props) {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [style, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [picture, setPic] = useState('');
    const handlePicChange = (event) => {
        const value = event.target.value;
        setPic(value);
    }

    const [location, setLoc] = useState('');
    const handleLocChange = (event) => {
        const value = event.target.value;
        setLoc(value)
    }

    const [locations, setLocs] = useState([]);
    const getData = async () => {
        const locUrl = 'http://localhost:8100/api/locations/';
        const response = await fetch(locUrl);
        if (response.ok) {
            const data = await response.json();
            setLocs(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, []);


    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.style = style
        data.fabric = fabric
        data.color = color;
        data.picture_url = picture;
        data.location = location

        const hatsUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            navigate('/hats');
        };

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-1">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" name="style" id="style" className="form-control"/>
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handlePicChange} value={picture} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control"/>
                            <label htmlFor="picture">Picture</label>
                        </div>
                        <div className="mb-1">
                            <select onChange={handleLocChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Select a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button>Add Hat</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm;

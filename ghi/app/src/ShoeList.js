import { useEffect, useState } from 'react';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import 'bootstrap/dist/css/bootstrap.min.css';

function ShoeColumn(props) {
    const [showModal, setShowModal] = useState(false);
    const [selectedShoeId, setSelectedShoeId] = useState(null);
    // const [removingShoe, setRemovingShoe] = useState(null);
    const [selectedGif, setSelectedGif] = useState('');

    const gifs = [
        'dog-cute.gif',
        'wylin-dog.gif',
        'dancing-doggy-excited-puppy.gif',
        'mascar-zapatilla-las-chicas-superpoderosas.gif',
        'shoe-saw.gif',
        'sonic-eating.gif',
        'dip-burning.gif'
    ]


    const handleDeleteClick = (shoeId) => {
        const randomGif = gifs[Math.floor(Math.random() * gifs.length)];
        setSelectedGif(randomGif);

        setShowModal(true);
        setSelectedShoeId(shoeId);
    };

    const handleCloseModal = () => {
        setShowModal(false);
    };

    const handleConfirmDelete = () => {
        handleCloseModal();
        props.onDelete(selectedShoeId);
    };

    return (
    <div className="col">
        {props.list.map(shoe => {
            console.log("shoe data:", shoe);
            // const shoe = data.shoe;
            return (
                <div key={shoe.pk} className={`card mb-3 shadow &{removingShoe === shoe.pk ? 'fadeAndScale': ''}`}>
                    <img
                    src={shoe.picture_url}
                    className="card-img-top"
                    alt={shoe.model_name}
                    />
                    <div className="card-body">
                    <h5 className="card-title">{shoe.model_name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                        {shoe.manufacturer}
                    </h6>
                    <h7 className="card-subtitle mb-2 text-muted">
                        {shoe.color}
                    </h7>
                    </div>
                    <div className="card-footer">
                        {shoe.bin.closet_name}
                    </div>
                    <button type="button" className="btn btn-danger" onClick={() => handleDeleteClick(shoe.pk)}>Delete</button>
                </div>
            );
        })}
        <Modal show={showModal} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Confirm Delete</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                    <img src={`/${selectedGif}`} alt="Dog Chewing Shoe Lace" style={{ width: '100%'}} />
            </Modal.Body>
            <Modal.Footer>
                <button onClick={handleCloseModal}>Cancel</button>
                <button onClick={handleConfirmDelete}>Confirm Delete</button>
            </Modal.Footer>
        </Modal>
        </div>
    );
}

function ShoeList(props) {
    const [shoeColumns, setShoeColumns] = useState([[],[],[]]);

    async function getShoes() {
        const url = 'http://localhost:8080/api/shoes/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let shoe of data.shoes) {
                    if (shoe.pk) {
                        console.log("shoe data:", shoe);
                        const detailUrl = `http://localhost:8080/api/shoes/${shoe.pk}/`;
                        requests.push(fetch(detailUrl));
                    } else {
                        console.error("shoe ID is undefined:", shoe);
                    }
                }

                const responses = await Promise.all(requests);
                const shoeColumns = [[], [], []];

                let i = 0;
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        shoeColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse);
                    }
                }
                setShoeColumns(shoeColumns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    const deleteShoe = async (shoeId) => {
        const deleteUrl = `http://localhost:8080/api/shoes/${shoeId}`;

        try {
            const response = await fetch(deleteUrl, { method: 'DELETE'});
            if (response.ok) {
                const updatedShoeColumns = shoeColumns.map(column =>
                    column.filter(shoe => shoe.pk !== shoeId)
                );
                setShoeColumns(updatedShoeColumns);
            } else {
                console.error("Sorry, we couldn't delete it:", response);
            }
        } catch (error) {
            console.error('Error deleting the shoe:', error);
        }
    };

    useEffect(() => {
        getShoes();
    }, []);

    return (
        <>
            <div className="px-3 py-3 text-center">
                <h1 className="display-5 fw-bold">Shoes!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">You'll never lose your shoes again, unless you forget to put them away.</p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"></div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                {shoeColumns.every(column => column.length === 0) ? (
                        <div className="text-center">
                            <p>You don't have any shoes yet.</p>
                            <Link to="/shoes/new" className="btn btn-primary">Add New Shoe</Link>
                        </div>
                    ) : (
                        shoeColumns.map((shoeList, index) => (
                            <ShoeColumn key={index} list={shoeList} onDelete={deleteShoe} />
                        ))
                    )}
                </div>
            </div>
        </>
    );
}

export default ShoeList;

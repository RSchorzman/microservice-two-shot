import { useEffect, useState } from 'react';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function HatTable(props) {
    const [showModal, setShowModal] = useState(false);
    const [selectedHatId, setSelectedHatId] = useState(null);
    const [selectedGif, setSelectedGif] = useState('');

    const gifs = [
        'dog-hat.gif',
        'dancing-lit.gif',
        'frog-hat.gif',
        'tossing-hat.gif',
        'cute-dog-watermelon.gif',
        'here-is-the-cap-assane-diop.gif'
    ]

    const handleDeleteClick = (hatID) => {
        const randomGif = gifs[Math.floor(Math.random() * gifs.length)];
        setSelectedGif(randomGif)
        setShowModal(true);
        setSelectedHatId(hatID);
    };

    const handleCloseModal = () => {
        setShowModal(false)
    };

    const handleConfirmDelete = () => {
        handleCloseModal();
        props.onDelete(selectedHatId);
    };

    return (
        <div className="col">
            {props.list.map(hat => {
            return (
                <div key={hat.pk} className={`card mb-3 shadow &{removingHat === hat.pk ? 'fadeAndScale': ''}`}>
                <img
                    src={hat.picture_url}
                    className="card-img-top"
                    alt='hat'
                />
                <div className="card-body">
                    <h5 className="card-title">{hat.name}</h5>
                    <p className="card-text">
                    {hat.style}/{hat.fabric}/{hat.color}
                    </p>
                    <h6 className="card-subtitle mb-2 text-muted">
                    {hat.location.closet_name}
                    </h6>
                    <button type="button" className="btn btn-danger" onClick={() => handleDeleteClick(hat.pk)}>Delete</button>
                </div>
                </div>
            );
            })}
            <Modal show={showModal} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Confirm Delete</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <img src={`/${selectedGif}`} alt="Gif" style={{ width: '100%'}} />
            </Modal.Body>
            <Modal.Footer>
                <button onClick={handleCloseModal}>Cancel</button>
                <button onClick={handleConfirmDelete}>Confirm Delete</button>
            </Modal.Footer>
            </Modal>
        </div>
    );
}

function HatsList(props) {
    const [hatsTable, setHatsTable] = useState([[],[],[]])

    const getHatData = async () => {
        const hatsUrl = 'http://localhost:8090/api/hats/';
        try {
            const responses = await fetch(hatsUrl)
            if (responses.ok) {
                const data = await responses.json();
                const requests = [];
                for (let hat of data.hats) {
                    console.log(data)
                    const hatUrl = `http://localhost:8090/api/hats/${hat.pk}`;
                    requests.push(fetch(hatUrl));
                }
                const response = await Promise.all(requests);
                const columns = [[], [], []];
                let index = 0;
                for (const hatResult of response) {
                    if (hatResult.ok) {
                        const hat = await hatResult.json();
                        columns[index].push(hat);
                        index = index + 1;
                        if (index > 2) {
                            index = 0;
                        }
                    } else{
                        console.error(hatResult);
                    }
                }
                setHatsTable(columns);
            }
        } catch (e) {
            console.error(e)
        }
    }

    const hatDelete = async (hatID) => {
        const deleteUrl = `http://localhost:8090/api/hats/${hatID}`
        try {
            const response = await fetch(deleteUrl, {method: "delete"});
            if (response.ok) {
                const updatedHatTable = hatsTable.map(columns =>
                    columns.filter(hat => hat.pk !== hatID)
                    );
                    setHatsTable(updatedHatTable);
            } else {
                console.error("No Deleteio")
            }
        } catch (e) {
            console.error(e)
        }
    };

    useEffect(() => {
        getHatData()
    }, [])

    return (
        <>
            <div className="px-3 py-3 text-center">
                <h1 className="display-5 fw-bold">Hats!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">New Hat, New You, Now Pick!</p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"></div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                {hatsTable.every(column => column.length === 0) ? (
                    <div className='text-center'>
                        <p>You don't have any hats yet!</p>
                        <Link to="/hats/new" className='btn btn-primary'>Add New Hat</Link>
                    </div>
                ) : (
                    hatsTable.map((hatList, index) => (
                        <HatTable key={index} list={hatList} onDelete={hatDelete}/>
                    ))
                )}
                </div>
            </div>
        </>
      );
}

export default HatsList

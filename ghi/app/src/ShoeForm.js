import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function ShoeForm() {
    const [bins, setBins] = useState([])
    const initialFormData = {
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
    }
    const [formData, setFormData] = useState(initialFormData);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();

        // console.log("Submitting data:", formData);
        const url = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            // setFormData(initialFormData);
            navigate('/shoes');
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Shoe</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={formData.manufacturer} />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" value={formData.model_name} />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
                            <label htmlFor='color'>Color</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
                            <label htmlFor='picture_url'>Picture URL</label>
                        </div>
                        <div className="mb-1">
                            <select onChange={handleFormChange} required name="bin" id="bin" className="form-control" value={formData.bin} >
                            <option value="">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <button type="submit">Add Shoe</button>
                    </form>
            </div>
            </div>
        </div>
    );
}

export default ShoeForm;
